import { CallsService } from './../services/calls.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calls',
  templateUrl: './calls.component.html',
  styleUrls: ['./calls.component.css']
})
export class CallsComponent implements OnInit {

  messages = [];
  errorMessage = "";
  
  constructor(private callsService : CallsService ) { }

  ngOnInit() {
  }

  getDataFinal(){
    this.callsService.getData().subscribe(

      data => {
        //const weatherItem = new WeatherItem(data.city.name, data.list[0].weather[0].description , data.list[0].main.temp, data.list[0].weather[0].icon,data.city.country);
        //this.weatherService.addWeatherItem(weatherItem);
        this.messages = data.messages;
      },
      err => {
        console.log(err);
      }

    );
  }

}
