import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class CallsService {

  constructor(private http : Http) { }

  url = "http://www.mocky.io/v2/59a6cd45100000ca0608fdcf";

  /// API call method
  getData() : Observable<any>{
     return this.http.get(this.url)
    .map(response => response.json())
    .catch(error => {
      alert("No results found!");
      console.error(error);
      return Observable.throw(error.json())
    });

  }


  
}
