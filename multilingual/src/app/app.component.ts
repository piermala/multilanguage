import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {Observable } from "rxjs";
import 'rxjs/Rx';
import {Routes, RouterModule} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  user = {
    name: 'James',
    age: 26
  };

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('it');
  }

  switchLanguage(language: string) {
    this.translate.use(language);
    console.log(this.translate.use(language));

    console.log(this.translate.instant("FirstObject.SecondObject"));
  }
  
}
  



