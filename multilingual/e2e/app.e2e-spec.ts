import { MultilingualPage } from './app.po';

describe('multilingual App', () => {
  let page: MultilingualPage;

  beforeEach(() => {
    page = new MultilingualPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
